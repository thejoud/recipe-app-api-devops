variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "thejoud1997@gmail.com"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  type    = string
  default = "recipe-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "508786416615.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  type        = string
  description = "ECR image for proxy"
  default     = "508786416615.dkr.ecr.us-east-2.amazonaws.com/recipe-api:latest"
}

variable "django_secret_key" {
  type        = string
  description = "Secret Key For Django App"
}