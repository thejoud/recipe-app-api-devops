terraform {
  backend "s3" {
    bucket  = "recipe-app-api-devops-terraform"
    key     = "recipe-app.tfstate"
    region  = "us-east-2"
    encrypt = true
    # define DynamoDB Table so we can not run terraform in 2 places at the same moment
    dynamodb_table = "receipe-aws-api"
  }
}

provider "aws" {
  region = "us-east-2"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {}