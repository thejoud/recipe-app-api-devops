# add mulltiple subnets to database
resource "aws_db_subnet_group" "main" {
  name = "${local.prefix}-main"
  subnet_ids = [
    aws_subnet.private_a.id,
    aws_subnet.private_b.id
  ]

  tags = merge(local.common_tags, tomap({ "Name" = "${local.prefix}-main" }))
}

resource "aws_security_group" "rds" {
  description = "Allow access to the RDS database instance"
  name        = "${local.prefix}-rds-inbound-access"
  vpc_id      = aws_vpc.main.id

  ingress {
    protocol  = "tcp"
    from_port = 5432
    to_port   = 5432

    # allow access from only this security groups
    security_groups = [
      aws_security_group.bastion.id,
      # give access to our ECS from database
      aws_security_group.ecs_service.id
    ]
  }

  tags = local.common_tags
}

# CREATE THE RDS Instance
resource "aws_db_instance" "main" {
  # database identifier
  identifier = "${local.prefix}-db"
  # name of the database inside the RDS instance
  name = "recipe"
  # Entry-level storage type in AWS --> cheapest
  allocated_storage = 20
  storage_type      = "gp2"
  # define the SQL engine
  engine         = "postgres"
  engine_version = "11.4"
  # type of database server we want to run
  instance_class = "db.t2.micro"
  # subnet group to assign this RDS to it
  db_subnet_group_name = aws_db_subnet_group.main.name
  # set the credentials of the database
  password = var.db_password
  username = var.db_username
  # how many days to create a database backup ---> 0 means it will not create
  backup_retention_period = 0
  # determins if the database will run on diffrent avaibility zones --> cost more
  # in real application always set to true
  multi_az = false
  # mechanism to protect data lose
  skip_final_snapshot = true
  # define security groups for this RDS
  vpc_security_group_ids = [aws_security_group.rds.id]

  tags = merge(local.common_tags, tomap({ "Name" : "${local.prefix}-main" }))
}