#!/bin/sh

# if there is any issue in the syntax exit the code and stop
set -e 

python manage.py collectstatic --noinput
python manage.py wait_for_db
python manage.py migrate

uwsgi --socket !9000 --workers 4 --master --enable-threads --module app.wsgi
uwsgi --master 